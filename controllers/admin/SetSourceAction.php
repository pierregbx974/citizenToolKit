<?php
class SetSourceAction extends CAction
{
    public function run($action=null, $set=null){
        $controller = $this->getController();
        $params = array();
        $params=$_POST;
        assert('!empty($_POST["type"]) != ""; //The element type is mandatory');
        assert('!empty($_POST["id"]) != ""; //The element Id is mandatory');
        assert('!empty($_POST["sourceKey"]) != ""; //The source key is mandatory');
        $res = array("result"=>false, "msg"=>Yii::t("common", "Something went wrong!"));
        try {
            if(!empty($action)){
                if($action=="add")
                    $res = Admin::addSourceInElement($_POST["id"], $_POST["type"], $_POST["sourceKey"], $set);
                if($action=="remove")
                    $res = Admin::removeSourceFromElement($_POST["id"], $_POST["type"], $_POST["sourceKey"], $set);
            }
        } catch (CTKException $e) {
            $res = array("result"=>false, "msg"=>$e->getMessage());
        }
        Rest::json($res);
    }
}

?>